
const FIRST_NAME = "Madalina";
const LAST_NAME = "Fota";
const GRUPA = "1077";

/**
 * Make the implementation here
 */
function numberParser(input, property) {

    var x=Number.MAX_SAFE_INTEGER+2;
    var y=Number.MIN_SAFE_INTEGER-2;
        if(isNaN(input)) return NaN;
            else
                if(typeof input==='string'|| input instanceof String) return parseInt(input);
                     else
                         if(!Number.isFinite(input)) return NaN;
                            else 
                                if(input===x ||input===y) return NaN;
                                    else
                                        return Math.floor(input);
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

